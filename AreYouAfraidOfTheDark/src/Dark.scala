
import java.io.File
import javax.imageio.ImageIO
import java.awt.image.BufferedImage

object Dark {
  def main(args: Array[String]): Unit = {

    //  find current directory of project to set relative path
    val mainDirectory = new File(".").getCanonicalPath

    //  getting list of files from input folder
    val inputFilesList = new File(mainDirectory + "/src/in").listFiles.toList;

    //  get all photos as array
    val photosCount = inputFilesList.length;
    var photos: Array[BufferedImage] = new Array[BufferedImage](photosCount);
    for (i <- 0 to (photosCount - 1)) {
      photos(i) = ImageIO.read(inputFilesList(i));
    }

    //  brightness verification of photos
    for (img <- 0 to photos.length - 1) {

      //  get image dimensions
      var imageWidth = photos(img).getWidth;
      var imageHeight = photos(img).getHeight;

      //  make array of brightness of all pixels
      var pixelsBrightness = Array.ofDim[Float](imageWidth, imageHeight);
      for (x <- 0 to imageWidth - 1; y <- 0 to imageHeight - 1) {
        pixelsBrightness(x)(y) = (((photos(img).getRGB(x, y) >> 16) & 0xFF) * 0.2126f + ((photos(img).getRGB(x, y) >> 8) & 0xFF) * 0.7152f + ((photos(img).getRGB(x, y) >> 0) & 0xFF) * 0.0722f) / 255;
      }

      //  calculate the average brightness of all pixels
      var totalBrightness: Float = 0;
      for (x <- 0 to imageWidth - 1; y <- 0 to imageHeight - 1) {
        totalBrightness += pixelsBrightness(x)(y);
      }
      var averageBrightness = totalBrightness / (imageWidth * imageHeight);

      //  validation of image and set right filename
      var imageParameter = ((1 - averageBrightness) * 100).toInt;

      val verificationParameter = 80;

      var inputFileName = inputFilesList(img).getName.split("\\.");

      var outputFileName: String = "";
      if (imageParameter < verificationParameter) {
        outputFileName = inputFileName(0) + "_bright_" + imageParameter.toString() + "." + inputFileName(1);

      } else {
        outputFileName = inputFileName(0) + "_dark_" + imageParameter.toString() + "." + inputFileName(1);
      }

      val outputFile = new File(mainDirectory + "/src/out/" + outputFileName);

      //  rename and move file
      inputFilesList(img).renameTo(outputFile);
    }
  }
}
